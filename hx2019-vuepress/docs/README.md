# Develop a Cross-platform App - Software Engineering Exercise

## goal
- setup the tool chain to build the [code](https://gitlab.com/yiwang/localnative) and run [Local Native App](https://localnative.app), which includes [desktop application, web browser extension](https://youtu.be/DBsVscpSp6w), [ios](https://youtu.be/3dhB5gTtXNM) and [android](https://play.google.com/store/apps/details?id=app.localnative).
- discover the story and people behind those tools.
- explore various software engineering concepts.

## non-goal
- in-depth coverage of any particular language, tool or algorithm.
- it's encouraged to hack the code to experiment adding new features or fix bugs, but not required.

## prerequisite
- able to setup and trouble shot software installation process.
- able to work with (and enjoy!) typing in non-gui command-line interface.
- able to fluently switch between multiple applications and track file locations.
- some programming experience is preferred.

## hardware
- a relatively modern laptop is required.
- minimum 4-core; 8 core cpu recommended.
- minimum 4 GB of RAM; 8 GB of RAM or more recommended.
- enough disk space (minimum 50GB free space).

## operating system
- only mac support ios development.
- Local Native desktop and android development on windows will be using [virtual machine](https://en.wikipedia.org/wiki/Virtual_machine).

|os           | browser<br>extension | desktop<br>application| ios   | android   |
|-------------|----|----|----|----|
|gnu/linux    | ✓  | ✓  |    | ✓  |
|macintosh    | ✓  | ✓  | ✓  | ✓  |
|windows      | ✓  | *  |    | *  |
