# [git](https://www.youtube.com/watch?v=4XpnKHJAok8)

[debian](https://www.debian.org/) based gnu/linux
```
sudo apt install git
```

mac
```
brew install git
```

clone repo
```
mkdir ~/repos/localnative
cd ~/repos/localnative
git clone https://gitlab.com/localnative/localnative.git
git clone https://gitlab.com/localnative/localnative-lectures.git
cd ~/repos/localnative/localnative
git log
```

register and add ssh pub key to
  - [gitlab](https://docs.gitlab.com/ee/ssh/)

[merge and rebase](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)
[squashing](http://gitready.com/advanced/2009/02/10/squashing-commits-with-rebase.html)
[tig](https://jonas.github.io/tig/)
[gitk](https://git-scm.com/docs/gitk)

## more
register and add ssh pub key to
  - [github](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key)
  - [sr.ht](https://meta.sr.ht/)

[GPL](https://drewdevault.com/2019/06/13/My-journey-from-MIT-to-GPL.html)
