# Summary

[Develop a Cross-platform App - Software Engineering Exercise](./hx2019.md)
[开发跨平台应用 - 软件工程实践](./hx2019-zh.md)
- [diagrams](./diagrams.md)
  - [diagram 1](./diagram-1.md)
  - [diagram 2](./diagram-2.md)
- [fall week 1 - 09/08](./fall-week-1.md)
  - [localnative app](./localnative_app.md)
  - [gitlab](./gitlab.md)
  - [gitter](./gitter.md)
  - [github](./github.md)
  - [downloads](./downloads.md)
- [fall week 2 - 09/15](./fall-week-2.md)
  - [gnu/linux](./gnu_linux.md)
  - [virtualbox](./virtualbox.md)
  - [pureos](./pureos.md)
  - [librem one](./librem_one.md)
- [fall week 3 - 09/22](./fall-week-3.md)
  - [cli](./cli.md)
  - [git](./git.md)
  - [dotfiles](./dotfiles.md)
- [fall week 4 - 09/29](./fall-week-4.md)
- [fall week 5 - 10/06](./fall-week-5.md)
- [fall week 6 - 10/20](./fall-week-6.md)
- [fall week 7 - 10/27](./fall-week-7.md)
  - [ssh](./ssh.md)
  - [rust](./rust.md)
  - [mdbook](./mdbook.md)
- [fall week 8 - 11/03](./fall-week-8.md)
  - [markdown](./markdown.md)
  - [html](./html.md)
  - [css](./css.md)
- [fall week 9 - 11/17](./fall-week-9.md)
  - [node.js](./node.js.md)
  - [javascript](./javascript.md)
  - [web extension](./web_extension.md)
- [fall week 10 - 11/24](./fall-week-10.md)
  - [sqlite](./sqlite.md)
  - [sql](./sql.md)
- [fall week 11 - 12/08](./fall-week-11.md)
  - [electron](./electron.md)
  - [qt](./qt.md)
- [fall week 12 - 12/15](./fall-week-12.md)
  - [desktop](./desktop.md)
  - [neon](./neon.md)
- [fall week 13 - 01/05](./fall-week-13.md)
  - [mobile](./mobile.md)
  - [xcode](./xcode.md)
  - [android studio](./android_studio.md)
- [fall week 14 - 01/12](./fall-week-14.md)
  - [ios](./ios.md)
  - [object-c](./object-c.md)
  - [swift](./swift.md)
- [fall week 15 - 01/26](./fall-week-15.md)
  - [android](./android.md)
  - [java](./java.md)
  - [kotlin](./kotlin.md)
- [spring week 1 - 02/02](./spring-week-1.md)
  - [dapp](./dapp.md)
  - [ssb](./ssb.md)
- [spring week 2 - 02/09](./spring-week-2.md)
  - [crud](./crud.md)
- [spring week 3 - 02/23](./spring-week-3.md)
  - [upgrade](./upgrade.md)
- [spring week 4 - 03/01](./spring-week-4.md)
  - [search](./search.md)
- [spring week 5 - 03/08](./spring-week-5.md)
  - [tag](./tag.md)
- [spring week 6 - 03/15](./spring-week-6.md)
  - [sync](./sync.md)
  - [uuid](./uuid.md)
- [spring week 7 - 03/22](./spring-week-7.md)
  - [tarpc](./tarpc.md)
- [spring week 8 - 03/29](./spring-week-8.md)
  - [docusaurus](./docusaurus.md)
  - [i18n](./i18n.md)
- [spring week 9 - 04/05](./spring-week-9.md)
  - [pagination](./pagination.md)
- [spring week 10 - 04/19](./spring-week-10.md)
  - [d3.js](./d3.js.md)
- [spring week 11 - 04/26](./spring-week-11.md)
  - [app store](./app_store.md)
  - [google play](./google_play.md)
  - [chrome web store](./chrome_web_store.md)
  - [firefox add-ons](./firefox_addons.md)
- [spring week 12 - 05/03](./spring-week-12.md)
  - [web hosting](./web_hosting.md)
  - [firebase](./firebase.md)
- [spring week 13 - 05/17](./spring-week-13.md)
- [spring week 14 - 05/31](./spring-week-14.md)
- [spring week 15 - 06/07](./spring-week-15.md)
- [spring week 16 - 06/14](./spring-week-16.md)


[Source Code](./source-code.md)
[License](./license.md)
