# xcode

[pod](https://guides.cocoapods.org/using/getting-started.html#getting-started)

```
gem install cocoapods --user-install
```

put `pod` command in `PATH`, in `.bashrc`, `.zshrc` or the like:
```
export PATH=$GEM_HOME/ruby/2.3.0/bin:$PATH
```

[Podfile](https://guides.cocoapods.org/using/using-cocoapods.html)
```
pod install
```
