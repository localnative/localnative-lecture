# diagram 1

<mermaid>
graph TD
IDE{IDE}-->Atom
IDE-->ToolBox
IDE-->Xcode
ToolBox-->AS[Android Studio]
ToolBox-->IDEA
IDEA-->rp[rust, vim plugin]
Mac((Mac))-->Xcode
Mac-->iTerm2
iTerm2-->Homebrew
Homebrew-->CLI{CLI}
CLI-->man
CLI-->ls
CLI-->cd
CLI-->pwd
CLI-->rm
CLI-->mkdir
CLI-->vim
CLI-->emacs
Windows((Windows))-->VirtualBox
VirtualBox-->PureOS
GNU((GUN/Linux))-->Debian
Debian-->PureOS
PureOS-->Terminator
Terminator-->CLI
PureOS-->apt
apt-->linux-headers
linux-headers-->VM[VM full screen]
</mermaid>
