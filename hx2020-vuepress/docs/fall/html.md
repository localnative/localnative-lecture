# html
[https://www.w3.org/TR/html52/](https://www.w3.org/TR/html52/)

[https://www.w3schools.com/html/](https://www.w3schools.com/html/)

## more
[https://mustache.github.io/](https://mustache.github.io/)

[http://jade-lang.com/](http://jade-lang.com/)

[http://handlebarsjs.com/](http://handlebarsjs.com/)
