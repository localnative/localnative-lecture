# [gnu/linux](https://www.gnu.org/gnu/linux-and-gnu.zh-cn.html)

[RMS](https://www.youtube.com/watch?v=VMM6D9vuHkY)

[linux](https://www.ted.com/talks/linus_torvalds_the_mind_behind_linux/discussion) [history](https://www.youtube.com/watch?v=UjDQtNYxtbU) [chart](https://commons.wikimedia.org/wiki/File:Linux_Distribution_Timeline.svg)

## more
[FLOSS](https://www.gnu.org/philosophy/floss-and-foss.en.html): [Free/Libre](https://www.youtube.com/watch?v=Ag1AKIl_2GM) [and](https://choosealicense.com/) [Open Source](https://www.youtube.com/watch?v=UIDb6VBO9os) [Software](https://spdx.org/licenses/)

[guix](https://www.gnu.org/software/guix/) vs [nixos](https://nixos.org/)
