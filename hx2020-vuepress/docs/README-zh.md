# 开发去中心跨平台应用 - 软件工程实践 2020-2021

## 目标
- 安装配置一系列开发工具，使能运行 [Local Native](https://localnative.app) 和 [Fastxt](https://fastxt.app) 的[代](https://gitlab.com/localnative/localnative)[码](https://gitlab.com/fastxt/fastxt)，涉及[桌面程序，浏览器扩展](https://youtu.be/DBsVscpSp6w)，[iOS](https://youtu.be/3dhB5gTtXNM)， [iPadOS](https://youtu.be/tZhbwV_a0fs) 和 [安卓](https://play.google.com/store/apps/details?id=app.localnative)开发。
- 发现这些工具背后的故事和人。
- 探索一些软件工程的概念。

## 非目标
- 不会深纠某一个具体的语言，工具和算法。
- 鼓励 hack 代码增加新特性，但不是必须。

## 基础
- 能安装软件并解决安装过程中的问题。
- 能（并喜欢！）在非图形的命令行界面中工作。
- 能熟练地在不同的软件之间切换，能准确定位各种文件的位置。
- 推荐有一定的编程经验。

## 硬件和操作系统要求
- 苹果电脑 和 最新的 macOS Catalina 操作系统 (不使用 Windows)
  - CPU: 4 核以上
  - 内存: 8 GB 以上
  - 磁盘空间: 50GB 以上空闲
- 移动设备
  - 苹果 iOS/iPadOS 版本 13 以上
  - 安卓 Android 移动设备（非必须）
