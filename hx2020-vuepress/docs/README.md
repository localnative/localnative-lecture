# Develop Decentralized Cross-platform Apps - Software Engineering Exercise 2020-2021

## Goal
- Setup the tool chain to build the [source](https://gitlab.com/localnative/localnative) [code](https://gitlab.com/fastxt/fastxt) and run [Local Native](https://localnative.app) and [Fastxt](https://fastxt.app), which includes [Desktop application, Web browser extension](https://youtu.be/DBsVscpSp6w), [iOS](https://youtu.be/3dhB5gTtXNM), [iPadOS](https://youtu.be/tZhbwV_a0fs)  and [Android](https://play.google.com/store/apps/details?id=app.localnative).
- Discover the story and people behind those tools.
- Explore various software engineering concepts.

## Non-goal
- In-depth coverage of any particular language, tool or algorithm.
- It's encouraged to hack the code to experiment adding new features, but not required.

## Prerequisite
- Able to setup and trouble shot software installation process.
- Able to work with (and enjoy!) typing in non-GUI command-line interface.
- Able to fluently switch between multiple applications and track file locations.
- Some programming experience is preferred.

## Hardware and Operating System Requirement
- Apple computer with new macOS Catalina OS (not use Windows)
  - CPU: 4-core or above
  - Memory: 8 GB RAM or above
  - Free disk space: 50 GB or above
- Mobile device
  - Apple iOS/iPadOS version 13 or above
  - Android mobile device (Optional)
